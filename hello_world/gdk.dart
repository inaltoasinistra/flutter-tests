// Copyright (c) 2019, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.
import 'dart:ffi';
import 'dart:io' show Platform, Directory;
import 'dart:isolate';

import 'package:path/path.dart' as path;
import 'package:ffi/ffi.dart';

typedef InitFunc = Pointer Function(Pointer json);
typedef Init = Pointer Function(Pointer json);

typedef ConvertFunc = Pointer<Utf8> Function(Pointer<Utf8> str, Pointer<Pointer> output);
typedef Convert = Pointer<Utf8> Function(Pointer<Utf8> str, Pointer<Pointer> output);

// GDK_API int GA_convert_json_to_string(const GA_json* json, char** output);
typedef ConvertRFunc = Int32 Function(Pointer json, Pointer<Pointer> output);
typedef ConvertR = int Function(Pointer json, Pointer<Pointer> output);

typedef DestroyFunc = Int32 Function(Pointer json);
typedef Destroy = int Function(Pointer json);

typedef ValidateMnemonicFunc = Int32 Function(Pointer mnemonic, Pointer<Int32> valid);
typedef ValidateMnemonic = int Function(Pointer mnemonic, Pointer<Int32> valid);

typedef CreateSessionFunc = Int32 Function(Pointer<Pointer> session);
typedef CreateSession = int Function(Pointer<Pointer> session);

typedef ConnectFunc = Int32 Function(Pointer session, Pointer netParams);
typedef Connect = int Function(Pointer session, Pointer netParams);

// GDK_DEFINE_C_FUNCTION_4(GA_login_user, struct GA_session*, session, const GA_json*, hw_device, const GA_json*, details,
//     struct GA_auth_handler**, call,
//     { *call = make_call(new ga::sdk::login_user_call(*session, *json_cast(hw_device), *json_cast(details))); })
typedef LoginUserFunc = Int32 Function(Pointer session, Pointer hwDevice, Pointer details, Pointer<Pointer> authHandler);
typedef LoginUser = int Function(Pointer session, Pointer hwDevice, Pointer details, Pointer<Pointer> authHandler);

// int GA_set_notification_handler(struct GA_session* session, GA_notification_handler handler, void* context)
typedef SetNotificationHandlerFunc = Int32 Function(Pointer session, Pointer<NativeFunction<NotificationHandler>> handler, Pointer context);
typedef SetNotificationHandler = int Function(Pointer session, Pointer<NativeFunction<NotificationHandler>> handler, Pointer context);
// typedef void (*GA_notification_handler)(void* context, GA_json* details);
typedef NotificationHandler = Void Function(Pointer, Pointer);

// GDK_DEFINE_C_FUNCTION_3(GA_get_balance, struct GA_session*, session, const GA_json*, details, struct GA_auth_handler**,
//     call, { *call = make_call(new ga::sdk::get_balance_call(*session, *json_cast(details))); })
typedef GetBalanceFunc = Int32 Function(Pointer session, Pointer details, Pointer<Pointer> authHandler);
typedef GetBalance = int Function(Pointer session, Pointer details, Pointer<Pointer> authHandler);

// int GA_auth_handler_get_status(struct GA_auth_handler* call, GA_json** output)
typedef AuthHandlerGetStatusFunc = Int32 Function(Pointer call, Pointer<Pointer> output);
typedef AuthHandlerGetStatus = int Function(Pointer call, Pointer<Pointer> output);

final dylib = getDylib();

final init = dylib.lookupFunction<InitFunc, Init>('GA_init');
final convert = dylib.lookupFunction<ConvertFunc, Convert>('GA_convert_string_to_json');
final convertR = dylib.lookupFunction<ConvertRFunc, ConvertR>('GA_convert_json_to_string');
final destroy = dylib.lookupFunction<DestroyFunc, Destroy>('GA_destroy_json');
final validateMnemonic = dylib.lookupFunction<ValidateMnemonicFunc, ValidateMnemonic>('GA_validate_mnemonic');
final createSession = dylib.lookupFunction<CreateSessionFunc, CreateSession>('GA_create_session');
final connect = dylib.lookupFunction<ConnectFunc, Connect>('GA_connect');
final loginUser = dylib.lookupFunction<LoginUserFunc, LoginUser>('GA_login_user');
final getBalance = dylib.lookupFunction<GetBalanceFunc, GetBalance>('GA_get_balance');
final setNotificationHandler = dylib.lookupFunction<SetNotificationHandlerFunc, SetNotificationHandler>('GA_set_notification_handler');
final authHandlerGetStatus = dylib.lookupFunction<AuthHandlerGetStatusFunc, AuthHandlerGetStatus>('GA_auth_handler_get_status');

void _connect(var args) async {
  final dylib = getDylib();
  final convert = dylib.lookupFunction<ConvertFunc, Convert>('GA_convert_string_to_json');
  final connect = dylib.lookupFunction<ConnectFunc, Connect>('GA_connect');

  print(100);
  final SendPort sendPort = args[0];
  final Pointer<Pointer> session = Pointer.fromAddress(args[1]);
  final Pointer<Pointer> netParams = malloc();

  print(101);
  // convert('{"name":"testnet", "spv_enabled": false, "proxy": "127.0.0.1:9999","use_tor": true}'.toNativeUtf8(), netParams);
  convert('{"name":"testnet", "spv_enabled": false}'.toNativeUtf8(), netParams);
  print(102);
  final state = connect(session.value, netParams.value);
  print('After connect: $state');
}

Future<void> _backgroundConnect(Pointer<Pointer> session) async {
  final p = ReceivePort();
  print(200);
  await Isolate.spawn(_connect, [p.sendPort, session.address]);
  print(201);
  return;
}

// typedef void (*GA_notification_handler)(void* context, GA_json* details);
void notificationHandler(Pointer context, Pointer details) {
  final Pointer<Pointer<Utf8>> jsonContext = malloc();
  convertR(context, jsonContext);
  final Pointer<Pointer<Utf8>> jsonString = malloc();
  convertR(details, jsonString);
  print('NOTIFY ${jsonString.value.toDartString()}');
  print('CONTEXT ${jsonContext.value.toDartString()}');
}

DynamicLibrary getDylib() {
  // Open the dynamic library
  var libraryPath = path.join(
      Directory.current.path, 'greenaddress', 'libgreenaddress.so');

  if (Platform.isMacOS) {
    libraryPath = path.join(
        Directory.current.path, 'greenaddress', 'libgreenaddress.dylib');
  }

  if (Platform.isWindows) {
    libraryPath = path.join(
        Directory.current.path, 'greenaddress', 'Debug', 'libgreenaddress.dll');
  }

  return DynamicLibrary.open(libraryPath);
}

void main() async {

  final initParams = '{"datadir": "/tmp/GA","tordir": "/tmp/GAtor",'
      '"registrydir": "/tmp/GAregistry","log_level": "debug"}';

  final Pointer<Pointer> pointer = malloc();
  convert(initParams.toNativeUtf8(), pointer);
  init(pointer.value);
  destroy(pointer.value);

  // const gdkMnemonic = 'order earth buddy render ocean produce bacon orchard congress law illness goat';
  const gdkMnemonic = 'tortoise mask cruel loud view park desert february bind alpha acid length';
  final Pointer<Int32> valid = malloc();
  validateMnemonic(gdkMnemonic.toNativeUtf8(), valid);
  print("Is mnemonic valid: ${valid.value}");

  final Pointer<Pointer> session = malloc();
  createSession(session);
  print(3);

  final Pointer<Pointer> notificationHandlerContext = malloc();
  convert('{"context":true}'.toNativeUtf8(), notificationHandlerContext);
  final handlerPointer = Pointer.fromFunction<NotificationHandler>(notificationHandler);
  setNotificationHandler(session.value, handlerPointer, notificationHandlerContext.value);

  // int Function(Pointer session, Pointer handler, Pointer context);
  // notificationHandler
  // setNotificationHandler(session.value, notificationHandler, notificationHandlerContext);

  final Pointer<Pointer> netParams = malloc();
  // convert('{"name":"electrum-testnet-liquid"}'.toNativeUtf8(), netParams);
  // {
  //    "name": "testnet",
  //    "proxy": "localhost:9150",
  //    "use_tor": true,
  //    "user_agent": "green_android v2.33",
  //    "spv_enabled": false,
  //    "cert_expiry_threshold": 1
  // }
  // 127.0.0.1:9999
  print(4);
  // convert('{"name":"testnet", "spv_enabled": false, "proxy": "127.0.0.1:9998","use_tor": true}'.toNativeUtf8(), netParams);
  convert('{"name":"electrum-testnet", "spv_enabled": false}'.toNativeUtf8(), netParams);
  print(5);
  // await _backgroundConnect(session);
  print(6);
  final state = connect(session.value, netParams.value);
  print('state $state');
  // print(6);
  // malloc.free(netParams);
  // print(7);
  // testIsolate();

  // LOGIN USER
  // int Function(Pointer<Utf8> session, Pointer<Utf8> hwDevice, Pointer<Utf8> details, Pointer<Pointer> authHandler);
  Pointer<Pointer> hwDevice = malloc();
  Pointer<Pointer> authHandler = malloc();
  // gdkMnemonic.toNativeUtf8()
  convert('{}'.toNativeUtf8(), hwDevice);
  final Pointer<Pointer> details = malloc();
  convert('{"mnemonic":"$gdkMnemonic","password":""}'.toNativeUtf8(), details);
  print(7);
  final loginRes = loginUser(session.value, hwDevice.value, details.value, authHandler);
  print('loginUser $loginRes');

  final Pointer<Pointer> balanceDetails = malloc();
  convert('{"subaccount":0,"num_confs":0}'.toNativeUtf8(), balanceDetails);
  final balanceRes = getBalance(session.value, balanceDetails.value, authHandler);
  print('balance $balanceRes');

  Pointer<Pointer> balanceStatus = malloc();
  authHandlerGetStatus(authHandler.value, balanceStatus);

  final Pointer<Pointer<Utf8>> jsonString = malloc();
  convertR(balanceStatus.value, jsonString);
  print('balance: ${jsonString.value.toDartString()}');
}

void downloadAndCompressTheInternet(List<Object> arguments) {
  SendPort sendPort = arguments[0] as SendPort;
  int number = arguments[1] as int;
  sendPort.send(42 + number);
}

void testIsolate() async {
  final receivePort = ReceivePort();

  final isolate = await Isolate.spawn(
    downloadAndCompressTheInternet,
    [receivePort.sendPort, 3],
  );

  receivePort.listen((message) {
    print('receivePort $message');
    receivePort.close();
    isolate.kill();
  });
}