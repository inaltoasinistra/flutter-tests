import 'dart:ffi';
import 'dart:convert';

import 'package:ffi/ffi.dart';

import 'types.dart' as types;

// typedef void (*GA_notification_handler)(void* context, GA_json* details);
void notificationHandler(Pointer<Utf8> context, Pointer<Utf8> details) {
  print('XXX notification');

  print(Gdk.toMap(context));
  print(Gdk.toMap(details));
}

void main() async {
  print('\nstart');
  final gdk = Gdk('electrum-testnet');
  const mnemonic = 'order earth buddy render ocean produce bacon orchard congress law illness goat';
  // const mnemonic = 'tortoise mask cruel loud view park desert february bind alpha acid length';
  bool isValid = gdk.validateMnemonic(mnemonic);
  print(isValid);
  print('LOGIN USER');
  // gdk.loginUser2(mnemonic);
}


class Gdk {
  String network;
  late Pointer<Pointer> session;
  static final encoder = JsonEncoder();
  static final decoder = JsonDecoder();

  Gdk(this.network) {
    init();
    createSession();
    setNotificationHandler();
    connect();
  }

  /// Converts map data to GA_json object, remember to destroy the GA_json
  static Pointer<Pointer<Utf8>> toGaJson(Map<String, dynamic> map) {
    final Pointer<Pointer<Utf8>> pointer = malloc();
    types.convertStringToJson(encoder.convert(map).toNativeUtf8(), pointer);
    return pointer;
  }

  /// Converts GA_json object to map, destroys the GA_json
  static Map<String, dynamic> toMapDestroy(Pointer<Pointer<Utf8>> gaJson) {
    final map = toMap(gaJson.value);
    destroyJson(gaJson);
    return map;
  }

  /// Converts GA_json object to map
  static Map<String, dynamic> toMap(Pointer<Utf8> gaJson) {
    final Pointer<Pointer<Utf8>> pointer = malloc();  // +1
    types.convertJsonToString(gaJson, pointer);
    final jsonString = pointer.value.toDartString();
    destroyJson(pointer);
    return decoder.convert(jsonString);
  }

  /// Destroy GA_json with gdk and free the pointer
  static void destroyJson(Pointer<Pointer<Utf8>> gaJson) {
    types.destroyJson(gaJson.value);
    malloc.free(gaJson);
  }

  void init() {
    final params = toGaJson({
      'datadir': '/tmp/GA',
      'tordir': '/tmp/GAtor',
      'registrydir': '/tmp/GAregistry',
      'log_level': 'debug',
    });  // +1
    types.init(params.value);
    destroyJson(params);  // -1
  }

  void createSession() {
    session = malloc();  // +1
    types.createSession(session);
    // TODO: destroy sesion
  }

  void setNotificationHandler() {
    final Pointer<Pointer<Utf8>> context = toGaJson({'context': true});  // +1
    final handlerPointer = Pointer.fromFunction<types.NotificationHandler>(notificationHandler);
    types.setNotificationHandler(session.value, handlerPointer, context.value);
    // FIXME: I don't know when context is used
    destroyJson(context);
  }

  void connect() {
    final netParams = toGaJson({
      'name': 'electrum-testnet',
      'spv_enabled': false,
    });  // + 1
    types.connect(session.value, netParams.value);
    destroyJson(netParams);
  }

  void loginUser(final String mnemonic) {
    final hwDevice = toGaJson({}); // +1
    Pointer<Pointer> authHandler = malloc();  // +2
    final details = toGaJson({'mnemonic': mnemonic, 'password': ''}); // +3
    // Int32 Function(Pointer session, Pointer hwDevice, Pointer details, Pointer<Pointer> authHandler);
    final res = types.loginUser(session.value, hwDevice.value, details.value, authHandler);
    print('>>> $res');
  }

  void loginUser2(final String mnemonic) {

    Pointer<Pointer> hwDevice = malloc();
    Pointer<Pointer> authHandler = malloc();
    // gdkMnemonic.toNativeUtf8()
    final res = types.convertStringToJson('{}'.toNativeUtf8(), hwDevice);
    print('>>> $res');
    final Pointer<Pointer> details = malloc();
    types.convertStringToJson('{"mnemonic":"$mnemonic","password":""}'.toNativeUtf8(), details);
    print(7);
    final loginRes = types.loginUser(session.value, hwDevice.value, details.value, authHandler);
    print('loginUser $loginRes');
  }

  bool validateMnemonic(final String mnemonic) {
    final mnemonicNative = mnemonic.toNativeUtf8();  // +1
    final Pointer<Int32> status = malloc();  // +2
    types.validateMnemonic(mnemonicNative, status);
    bool isValid = status.value == 1;
    malloc.free(mnemonicNative);  // -1
    malloc.free(status);  // -2
    return isValid;
  }
}

// void fInit() {
//   final params = fToGaJson({
//     'datadir': '/tmp/GA',
//     'tordir': '/tmp/GAtor',
//     'registrydir': '/tmp/GAregistry',
//     'log_level': 'debug',
//   });  // +1
//   types.init(params.value);
//   fDestroyJson(params);  // -1
// }
//
// bool fValidateMnemonic(String mnemonic) {
//   final mnemonicNative = mnemonic.toNativeUtf8();  // +1
//   final Pointer<Int32> status = malloc();  // +2
//   types.validateMnemonic(mnemonicNative, status);
//   bool isValid = status.value == 1;
//   // malloc.free(mnemonicNative);  // -1
//   // malloc.free(status);  // -2
//   return isValid;
// }
//
// /// Converts map data to GA_json object, remember to destroy the GA_json
// Pointer<Pointer<Utf8>> fToGaJson(Map<String, dynamic> map) {
//   final Pointer<Pointer<Utf8>> pointer = malloc();
//   types.convertStringToJson(JsonEncoder().convert(map).toNativeUtf8(), pointer);
//   return pointer;
// }
//
// /// Converts GA_json object to map, destroys the GA_json
// Map<String, dynamic> fToMap(Pointer<Pointer<Utf8>> gaJson) {
//   final Pointer<Pointer<Utf8>> pointer = malloc();
//   types.convertJsonToString(gaJson.value, pointer);
//   final jsonString = pointer.value.toDartString();
//   return JsonDecoder().convert(jsonString);
// }
//
// /// Destroy GA_json with gdk and free the pointer
// void fDestroyJson(Pointer<Pointer<Utf8>> gaJson) {
//   types.destroyJson(gaJson);
//   malloc.free(gaJson);
// }
//
// // FROM gdk.dart
//
// void copyOfGdk() {
//
//   plainInit();
//
//   return;
//   const gdkMnemonic = 'tortoise mask cruel loud view park desert february bind alpha acid length';
//   const gdkMnemonicWrong = 'tortoise mask cruel loud view park desert february bind alpha acid length CIAO';
//   validate(gdkMnemonic);
//   validate(gdkMnemonic);
//   validate(gdkMnemonic);
//   validate(gdkMnemonic);
// }
//
// void plainInit() {
//
//   final params = fToGaJson({
//     'datadir': '/tmp/GA',
//     'tordir': '/tmp/GAtor',
//     'registrydir': '/tmp/GAregistry',
//     'log_level': 'debug',
//   });  // +1
//   types.init(params.value);
//   types.destroyJson(params.value);
//   malloc.free(params);
//   print('init done');
// }
//
// void validate(String mnemonic) {
//   // const gdkMnemonic = 'order earth buddy render ocean produce bacon orchard congress law illness goat';
//   const gdkMnemonic = 'tortoise mask cruel loud view park desert february bind alpha acid length';
//   final Pointer<Int32> valid = malloc();
//   types.validateMnemonic(mnemonic.toNativeUtf8(), valid);
//   print("Is mnemonic valid: ${valid.value}");
//   malloc.free(valid);
// }
