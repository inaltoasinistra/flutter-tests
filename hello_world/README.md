
## Run the test code

In hello_library:

```bash
cmake .
make
```

In hello_world:

```bash
dart pub get
dart run <filename>.dart
```
