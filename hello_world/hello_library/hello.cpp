// Copyright (c) 2019, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

#include <stdio.h>
#include "hello.h"

#include "nlohmann/json.hpp"

using json = nlohmann::json;


int main()
{
    hello_world();
    return 0;
}

// Note:
// ---only on Windows---
// Every function needs to be exported to be able to access the functions by dart.
// Refer: https://stackoverflow.com/q/225432/8608146
void hello_world()
{
    printf("Hello World\n");
}

int sum_2_int(int a, char b)
{
    return a + b;
}

char *capitalize(char *str) {
   static char buffer[1024];
   strcpy(buffer, str);
   buffer[0] = buffer[0] + 1;
   return buffer;
}


int subtract(int* a, int b) {
    *a -= b;
    return 12;
}

char *create_string() {
    static char x[] = "UNA STRINGA";
    return x;
}

char *reverse(char *str, int length)
{
    // Allocates native memory in C.
    char *reversed_str = (char *)malloc((length + 1) * sizeof(char));
    for (int i = 0; i < length; i++) {
        reversed_str[length - i - 1] = str[i];
    }
    reversed_str[length] = '\0';
    return reversed_str;
}

void free_string(char *str)
{
    // Free native memory in C which was allocated in C.
    free(str);
}
