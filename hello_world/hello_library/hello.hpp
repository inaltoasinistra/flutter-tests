// Copyright (c) 2019, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

void hello_world();
int sum_2_int(int a, char b);
char *capitalize(char *str);
int subtract(int* a, int b);
char *create_string();
char *reverse(char *str, int length);
void free_string(char *str);
