// Copyright (c) 2019, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.
import 'dart:ffi';
import 'dart:io' show Platform, Directory;

import 'package:path/path.dart' as path;
import 'package:ffi/ffi.dart';

// FFI signature of the hello_world C function
typedef HelloWorldFunc = Void Function();
// Dart type definition for calling the C foreign function
typedef HelloWorld = void Function();

// FFI signature of the sum_2_int C function
typedef Sum2IntFunc = Int32 Function(Int32 a, Char b);
// Dart type definition for calling the C foreign function
typedef Sum2Int = int Function(int a, int b);

// // FFI signature of the sum_2_int C function
// typedef CapitalizeFunc = Pointer<Utf8> Function(Pointer<Utf8> a);
// // Dart type definition for calling the C foreign function
// typedef Capitalize = int Function(int a, int b);

typedef SubtractFunc = Int32 Function(Pointer<Int32> a, Int32 b);
typedef Subtract = int Function(Pointer<Int32> a, int b);

typedef CreateString = Pointer<Utf8> Function();

typedef ReverseFunc = Pointer<Utf8> Function(Pointer<Utf8> str, Int32 length);
typedef Reverse = Pointer<Utf8> Function(Pointer<Utf8> str, int length);

typedef FreeStringFunc = Void Function(Pointer<Utf8> str);
typedef FreeString = void Function(Pointer<Utf8> str);

// Example of handling a simple C struct
class Coordinate extends Struct {
  @Double()
  external double latitude;

  @Double()
  external double longitude;
}


void main() {
  // Open the dynamic library
  var libraryPath =
      path.join(Directory.current.path, 'hello_library', 'libhello.so');

  if (Platform.isMacOS) {
    libraryPath =
        path.join(Directory.current.path, 'hello_library', 'libhello.dylib');
  }

  if (Platform.isWindows) {
    libraryPath = path.join(
        Directory.current.path, 'hello_library', 'Debug', 'hello.dll');
  }

  final dylib = DynamicLibrary.open(libraryPath);

  // Look up the C function 'hello_world'
  final HelloWorld hello = dylib
      .lookup<NativeFunction<HelloWorldFunc>>('hello_world')
      .asFunction();
  // Look up the C function 'hello_world'
  final Sum2Int sum2Int = dylib
      .lookup<NativeFunction<Sum2IntFunc>>('sum_2_int')
      .asFunction();

  // final _capitalize = dylib.lookup<NativeFunction<Pointer<ffi.Utf8> Function(Pointer<Utf8>)>>('capitalize');
  // var capitalize = _capitalize.asFunction<Pointer<Utf8> Function(Pointer<Utf8>)>();

  final Subtract subtract = dylib
      .lookup<NativeFunction<SubtractFunc>>('subtract')
      .asFunction();

  // Call the function
  hello();
  print(sum2Int(23, 5));

  // Create a pointer
  final p = calloc<Int32>();
  final Pointer<Int32> q = calloc();
  // print('X ${q.toDartString()}');
  // Place a value into the address
  p.value = 30;

  final createString = dylib.lookupFunction<CreateString, CreateString>('create_string');

  final reverse = dylib.lookupFunction<ReverseFunc, Reverse>('reverse');
  final freeString = dylib.lookupFunction<FreeStringFunc, FreeString>('free_string');


  print(p.value);
  subtract(p, 7);
  print(p.value);
  print('create');
  final message = createString().toDartString();
  print(message);
  // print(capitalize('banana'));

  final backwards = 'backwards';
  final backwardsUtf8 = backwards.toNativeUtf8();
  final reversedMessageUtf8 = reverse(backwardsUtf8, backwards.length);
  final reversedMessage = reversedMessageUtf8.toDartString();
  calloc.free(backwardsUtf8);
  print('$backwards reversed is $reversedMessage');
  freeString(reversedMessageUtf8);
}
